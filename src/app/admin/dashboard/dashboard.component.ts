import { Component, OnInit } from '@angular/core';
import { state, style, animate, transition, trigger } from '@angular/animations';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations:[
    trigger('myAnimation',[
      state(
        'open',
        style(
        {
          opacity:1
        }
        )
      ),
      state('close',style(
        {
          translate:'translate(-100px)'
        }
      )),
      transition('open => closed', [
        animate('1s')
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  isOpen:boolean=true;
 



  

  

}
