import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  data:any={};
  constructor() { }

  ngOnInit(): void {
  }

  submitData()
  {
    console.log(this.data);
  }

}
