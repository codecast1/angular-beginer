import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  //inisial untuk data formulir
  user:any={};
  //constructor
  constructor(
    public api:ApiService,
    public router:Router
  ) { }
  //fungsi inisial, dijalankan ketika class ini dipanggil
  ngOnInit(): void {
  }
  //form validation
  email = new FormControl('', [Validators.required, Validators.email]);
  password=new FormControl('',[Validators.required]); 
  
  //register
  loading:boolean;
  login(user)
  {
    this.loading=true;
    this.api.login(user.email, user.password).subscribe(res=>{      
      this.loading=false;
      localStorage.setItem('appToken',JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    },err=>{
      this.loading=false;
      alert('Tidak dapat login');
    });
  }

}
