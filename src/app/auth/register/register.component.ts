import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  //inisial untuk data formulir
  user:any={};
  hide:boolean=true;
  //constructor
  constructor(
    public api:ApiService,
    public router:Router
  ) { }
  //fungsi inisial, dijalankan ketika class ini dipanggil
  ngOnInit(): void {
  }
  //form validation
  email = new FormControl('', [Validators.required, Validators.email]);
  password=new FormControl('',[Validators.minLength(6), Validators.required]); 
  
  //register
  loading:boolean;
  register(user)
  {
    this.loading=true;
    this.api.register(user.email, user.password).subscribe(res=>{
      this.loading=false;
      alert('Registrasi berhasil');
      this.router.navigate(['auth/login']);
    },err=>{
      this.loading=false;
      alert('Ada masalah..');
    });
  }

}
